package com.example.cinescape.Models;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cinescape.R;

public class InfoRenta extends AppCompatActivity {

    TextView txtTitulo, txtDescribe, txtCorreos, txtTelefono;
    Button btnLlamada, btnCancelar, btnVolverAI;
    private final int PHONE_CALL_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_renta);


        //recibo nombre desde el main
        Bundle bundle = getIntent().getExtras();
        String nombre = bundle.getString("TITULO");

        txtTitulo = (TextView)findViewById(R.id.txtTitulo);
        txtDescribe = (TextView)findViewById(R.id.txtDescribe);
        txtCorreos = (TextView)findViewById(R.id.txtCorreos);
        txtTelefono = (TextView)findViewById(R.id.txtTelefono);
        btnLlamada = (Button)findViewById(R.id.btnLlamada);
        btnCancelar = (Button)findViewById(R.id.btnCancelar);
        btnVolverAI = (Button)findViewById(R.id.btnVolverAI);


        // configuracion y conexion a bd
        try (GestorBD bd = new GestorBD(getApplicationContext())) {
            String buscar = nombre;
            String[] datos;
            datos=bd.buscaPelicula(buscar);
            //mostrar la info almacenada en datos resultado de la consulta
            txtTitulo.setText(datos[0]);
            txtDescribe.setText(datos[1]);
            txtCorreos.setText(datos[2]);
            txtTelefono.setText(datos[3]);

            btnLlamada.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String tel = txtTelefono.getText().toString();
                    if (tel != null){
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                            requestPermissions(new String[]{Manifest.permission.CALL_PHONE},PHONE_CALL_CODE);
                        }else{
                            versionAntigua(tel);
                        }
                    }
                }

                private void versiomNueva(){

                }
                private  void versionAntigua(String tel){
                    Intent intentLlamar = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+tel));
                    if (verificarPermisos(Manifest.permission.CALL_PHONE)){
                        startActivity(intentLlamar);
                    }else{
                        Toast.makeText(InfoRenta.this, "Active el permiso de llamadas", Toast.LENGTH_SHORT);
                    }
                }
            });

            btnCancelar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bd.eliminarPelicula(nombre);
                    Toast.makeText(getApplicationContext(), "SE CANCELO LA RENTA DE "+nombre, Toast.LENGTH_SHORT).show();
                    finish();
                }
            });
            btnVolverAI.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode){
            case PHONE_CALL_CODE:
                String permission = permissions[0];
                int result = grantResults[0];
                if (permission.equals(Manifest.permission.CALL_PHONE)){

                    if (result == PackageManager.PERMISSION_GRANTED){
                        String numTel = txtTelefono.getText().toString();
                        Intent llamada = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + numTel));
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) return;
                        startActivity(llamada);
                    }else {
                        Toast.makeText(this, "No haz autorizado el permiso", Toast.LENGTH_SHORT).show();
                    }
                }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private boolean verificarPermisos(String permiso){
        int resultado = this.checkCallingOrSelfPermission(permiso);
        return resultado == PackageManager.PERMISSION_GRANTED;
    }
}