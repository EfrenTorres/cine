package com.example.cinescape.Interfases;

import com.example.cinescape.Models.Post;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface PostService {
    @GET("comments")
    Call<List<Post>> find(@Query("q")String q);
}
