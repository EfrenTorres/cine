package com.example.cinescape.Models;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import com.example.cinescape.R;

import java.util.ArrayList;

public class Renta extends AppCompatActivity {

    ListView lvRentado;
    ArrayList<String> lista;
    ArrayAdapter adaptador;
    Button btnVolverA;

    @Override
    public void onResume() {
        super.onResume();
        setContentView(R.layout.activity_renta);

        btnVolverA =(Button)findViewById(R.id.btnVolverA);

        //configuracion lista y conexion a bd
        lvRentado = (ListView) findViewById(R.id.lvRentado);
        GestorBD bd = new GestorBD(getApplicationContext());
        lista = bd.datos_lvPelicula();
        adaptador = new ArrayAdapter(this, android.R.layout.simple_list_item_1,lista);
        lvRentado.setAdapter(adaptador);

        btnVolverA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        //enviar nombre a detalle para hacer consulta
        lvRentado.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> AdapterView, View view, int position, long l) {
                Intent intent = new Intent(Renta.this, InfoRenta.class);
                intent.putExtra("TITULO", lista.get(position));
                startActivity(intent);
            }
        });
    }
}