package com.example.cinescape.Models;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class GestorBD extends SQLiteOpenHelper {
    private static final String NOMBRE_BD = "Rentados";
    private static final int VERSION_BD = 1;
    private static final String TABLA_PELICULA = "CREATE TABLE PELICULA(TITULO TEXT PRIMARY KEY, DESCRIPCION TEXT, CORREO TEXT, TELEFONO TEXT)";
    public GestorBD (Context context){
        super(context, NOMBRE_BD, null, VERSION_BD);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(TABLA_PELICULA);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLA_PELICULA);
        sqLiteDatabase.execSQL(TABLA_PELICULA);
    }
    //datos predefinidos
    public void agregarPre(){
        SQLiteDatabase bd=getWritableDatabase();
        if (bd!=null){
            bd.execSQL("delete from PELICULA");
            bd.execSQL("INSERT INTO PELICULA VALUES('EL RESPLANDOR', 'PELICULA EN INGLES', 'alexis@mail.com','55-0000-0000')");
            bd.execSQL("INSERT INTO PELICULA VALUES('SANTOS DUMONT', 'PELICULA JAPON', 'santo@mail.com','55-0000-0000')");
            bd.close();
        }
    }

    // CRUD PARA PRODUCTOS
    public void agregarPelicula(String titulo, String descripcion, String correo, String telefono){
        SQLiteDatabase bd=getWritableDatabase();
        if (bd!=null){
            bd.execSQL("INSERT INTO PELICULA VALUES('"+titulo+"', '"+descripcion+"', '"+correo+"','"+telefono+"')");
            bd.close();
        }
    }

    public void eliminarPelicula(String titulo){
        SQLiteDatabase bd=getWritableDatabase();
        if (bd!=null){
            bd.execSQL("DELETE FROM PELICULA WHERE titulo='"+titulo+"'");
            bd.close();
        }
    }
    public ArrayList datos_lvPelicula(){
        ArrayList<String> lista = new ArrayList<>();
        SQLiteDatabase bd = this.getReadableDatabase();
        String q = "SELECT * FROM PELICULA";
        Cursor registros = bd.rawQuery(q,null);
        if (registros.moveToFirst()){
            do {
                String pelicula = registros.getString(0);
                lista.add(pelicula);
            }while(registros.moveToNext());
        }
        return lista;
    }
    public  String[] buscaPelicula(String buscar) {
        String[] datos = new String[4];
        SQLiteDatabase bd = this.getWritableDatabase();
        String q = "SELECT * FROM PELICULA WHERE TITULO ='"+buscar+"'";
        Cursor registros = bd.rawQuery(q,null);
        if (registros.moveToFirst()){
            for (int i = 0; i <= 3; i++) {
                datos[i] = registros.getString(i);
            }
        }
        return datos;
    }

    }