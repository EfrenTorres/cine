package com.example.cinescape.Models;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cinescape.R;

public class MainActivity extends AppCompatActivity {

    EditText edtUsuario, edtContrasena;
    TextView txtInfoU, txtInfoC;
    Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtUsuario = (EditText)findViewById(R.id.edtUsuario);
        edtContrasena = (EditText)findViewById(R.id.edtContrasena);
        txtInfoU = (TextView)findViewById(R.id.txtInfoU);
        txtInfoC = (TextView)findViewById(R.id.txtInfoC);
        btnLogin = (Button)findViewById(R.id.btnLogin);


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edtUsuario.getText().toString().isEmpty() || edtContrasena.getText().toString().isEmpty()){
                    if (edtUsuario.getText().toString().isEmpty()) txtInfoU.setText("CAMPO USUARIO VACIO");
                    if (edtContrasena.getText().toString().isEmpty()) txtInfoC.setText("CAMPO CONTRASEÑA VACIO");
                    Toast.makeText(MainActivity.this, "EXISTEN CAMPOS VACIOS", Toast.LENGTH_LONG).show();
                }else{
                    if (edtUsuario.getText().toString().equals("CineMax") && edtContrasena.getText().toString().equals("12345")) {
                        Toast.makeText(MainActivity.this, "BIENVENIDO", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(MainActivity.this, Catalogo.class);
                        startActivity(intent);
                        edtUsuario.setText("");
                        edtContrasena.setText("");
                    }else {
                        if (edtUsuario.getText().toString().equals("CineMax")) { } else { txtInfoU.setText("USUARIO INVALIDO"); }
                        if (edtContrasena.getText().toString().equals("12345")) { } else  { txtInfoC.setText("CONTRASEÑA INVALIDA"); }
                        Toast.makeText(MainActivity.this, "USUARIO O CONTRASEÑA INCORRECTOS", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

    }
}